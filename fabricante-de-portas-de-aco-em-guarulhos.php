<?php
    $title       = "Fabricante de Portas de Aço em Guarulhos";
    $description = "A maior fabricante de portas de aço em Guarulhos e São Paulo, oferecendo serviços a custo benefício incrível para toda região. Entre em contato e saiba mais!";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Procurando por uma <strong>fabricante de portas de aço em Guarulhos</strong>? A Central de Portas de Aço é uma empresa especializada em portas e portões de aço, automáticos ou manuais, com funcionamento preciso, produtos de qualidade e todo suporte pós venda. Oferecemos serviços completos para você que já possui seus portões e portas de aço, e para você que está pensando em adquirir um para sua casa ou residência, nós possuímos as melhores ofertas e condições. Faça um orçamento com a Central das Portas de Aço, a maior <strong>fabricante de portas de aço em Guarulhos</strong> e região.</p>
<p>Com clientes em todo Brasil e atendendo a grandes marcas como Taco Bell, Marisa, entre outros, nossa <strong>fabricante de portas de aço em Guarulhos</strong> faz com todo primor, a confecção de portões e portas de forma projetada, sob medida e personalizada. Você pode optar por funcionamentos diferentes, acionamento automático, manual, por controle remoto, com sensores, entre outros. Conheça nossos produtos e serviços e garanta segurança e praticidade ao comprar seus portões e portas de aço.</p>
<p>Atendemos residências, empresas, indústrias, lojas, shoppings, entre outros. Consulte nosso catálogo de produtos e serviços e contrate a <strong>fabricante de portas de aço em Guarulhos</strong> que mais possui casos de clientes satisfeitos na região.</p>
<h2><strong>A fabricante de portas de aço em Guarulhos que faz a diferença</strong></h2>
<p>Em nosso site você encontra diversas informações precisas sobre nossa<strong> fabricante de portas de aço em Guarulhos</strong>. Além de muita informação, você pode consultar a média de preço dos nossos produtos e serviços e falar conosco rapidamente para orçamentos precisos, agendamento de visita técnica, entre outros. Contrate nossos serviços de fabricante, manutenção e reparo de portas de aço para toda região de São Paulo.</p>
<h2><strong>Alto padrão, qualidade e luxo em nossa fabricante de portas de aço em Guarulhos</strong></h2>
<p>Nossa empresa preza sempre por entregar produtos e projetos com acabamento refinado, controle rigoroso de alto padrão de qualidade, design único, exclusivo, moderno e funcional. A maior <strong>fabricante de portas de aço em Guarulhos</strong> e São Paulo, oferecendo serviços a custo benefício incrível para toda região. Portas de enrolar automática (Portão RollUp ou Rollmatic), portas basculantes, guilhotina e muito mais estruturas em aço para você.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>