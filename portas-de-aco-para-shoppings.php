<?php
    $title       = "Portas de Aço Para Shoppings";
    $description = "Além de portas de aço para shoppings, oferecemos serviço de instalação (para a capital de São Paulo) e enviamos um kit de instalação para vendas fora do estado.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura por ótimas opções de <strong>portas de aço para shoppings </strong>encontrou o lugar com a maior variedade de produtos de alta qualidade do mercado.  As <strong>portas de aço para shoppings </strong>são um produto de grande importância para a proteção do seu comércio ou patrimônio de forma eficiente. Contamos com grandes opções para valorizar e proteger sua vitrine de forma com que seus produtos principais continuem a mostra mesmo com a loja fechada em alguns horários de pico de outros serviços do local. Na Central Portas oferecemos uma grande variedade com opções incríveis para ressaltar e valorizar ainda mais a sua loja. Nossos produtos buscam unir utilidade com segurança para oferecer um produto de grande utilidade em diversos comércios ao longo do Brasil. Não feche sua compra de portas ou portões de aço em outro lugar sem antes conhecer as opções e condições exclusivas que oferecemos para nossos clientes. Todos os nossos produtos são de fabricação própria, oferecendo um preço mais em conta com um alto controle de qualidade realizado pela nossa empresa.</p>
<p>Na Central Portas possuímos uma grande experiência com aço trabalhando com esse material desde 1999. Em 2013 nossa empresa se especializou na produção, distribuição e instalação de portas e portões de aço visando o potencial crescimento do ramo em nossa região. Compre suas <strong>portas de aço para shoppings </strong>no lugar mais recomendado do segmento.  Dentre nossos clientes contamos com grandes nomes como Marisa, Taco Bell, Besni e outras grandes empresas que contam com nossos produtos para a primeira proteção de seu patrimônio. Além de <strong>portas de aço para shoppings, </strong>oferecemos serviço de instalação (para a capital de São Paulo) e enviamos um kit de instalação para vendas fora do estado. Também oferecemos instalação de mezanino industrial e comercial.</p>
<h2><strong>As melhores portas de aço para shoppings estão na Central Portas.</strong></h2>
<p>Nossa empresa se preocupa em oferecer opções de <strong>portas de aço para shoppings </strong>com muita utilidade sem se esquecer de uma estética trabalhada de maneira para valorizar a fachada de nossos clientes tendo em vista que muitas vezes são a primeira impressão de uma empresa.</p>
<h2><strong>Entre já em contato e saiba mais sobre as portas de aço para shoppings.</strong></h2>
<p>Para eventuais dúvidas sobre as <strong>portas de aço para shoppings </strong>ou quaisquer outros produtos ou serviços oferecidos por nossa empresa entre em contato e seja auxiliado por um de nossos atendentes.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>