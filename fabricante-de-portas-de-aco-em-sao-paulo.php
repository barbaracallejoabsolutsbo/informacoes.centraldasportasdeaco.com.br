<?php
    $title       = "Fabricante de Portas de Aço em São Paulo";
    $description = "A fabricante de portas de aço em São Paulo, Central das Portas de Aço, é uma empresa que confecciona estruturas em aço a nível industrial para conforto e segurança dos usuários. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Conheça nossa empresa. A <strong>fabricante de portas de aço em São Paulo</strong>, Central das Portas de Aço, é uma empresa que confecciona estruturas em aço a nível industrial para conforto e segurança dos usuários. Entre nossos produtos é possível encontrar mezaninos, portas de aço nos modelos de enrolar (RollUp), guilhotina e basculante, podendo contar com automatização por controle remoto e motores exclusivos.</p>
<p>Aqui você também tem todo suporte de manutenção e serviços de pequenos reparos para esses tipos de estruturas, oferecemos mão de obra especializada e muitas opções de reposição de peças e equipamentos para automação de portões e estruturas industriais. Você pode fazer todo projeto, instalação e manutenção com nossa <strong>fabricante de portas de aço em São Paulo.</strong></p>
<p>Acessórios para portas de enrolar, portas de enrolar residenciais, comerciais e industriais, porta basculante, porta guilhotina e muito mais. Confira o catálogo disponível em nosso site e colete todas informações sobre serviços e produtos disponíveis na maior <strong>fabricante de portas de aço em São Paulo</strong>, Central das Portas de Aço.</p>
<p>Nossa empresa é totalmente comprometida com a segurança de nossos clientes, prezamos por produtos com funcionamento perfeito, design sempre bem acabado e satisfação garantida. A Central das Portas de Aço é uma <strong>fabricante de portas de aço em São Paulo </strong>que trabalha com aço galvanizado de alta qualidade, liga metálica altamente resistente e durável, segura e perfeita para projetos de estruturas industriais, portões e portas.</p>
<h2><strong>Confiabilidade e segurança, fabricante de portas de aço em São Paulo</strong></h2>
<p>Estruturas industriais, mezaninos, coberturas, portas de aço de enrolar e muito mais, com confiabilidade, segurança e toda praticidade com automação. Conheça a Central das Portas de Aço. Uma<strong> fabricante de portas de aço em São Paulo </strong>onde você pode comprar, projetar, instalar e efetuar muitos serviços para esses tipos de estruturas em aço acima citados. Faça um orçamento e contrate nossos serviços especializados para a área.</p>
<h2><strong>Conserto e manutenção em nossa fabricante de portas de Aço em São Paulo</strong></h2>
<p>Serviços exclusivos para clientes de São Paulo. A Central das Portas de Aço oferece as melhores condições, ofertas e preços para manutenção, reparos em geral, peças de reposição, mão de obra especializada para conserto de portas de aço de enrolar, portas basculantes e portas guilhotina. A<strong> fabricante de portas de aço em São Paulo </strong>oferece serviços completos para você a nível industrial, comercial ou residencial. Confira.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>