<?php
    $title       = "Venda de Portas de Aço";
    $description = "Para eventuais dúvidas sobre a nossa venda de portas de aço entre em contato e seja prontamente auxiliado por um especialista para te atender da melhor maneira possível.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura a maior loja de <strong>venda de portas de aço </strong>encontrou o lugar ideal. A Central Portas é fabricante de todos os produtos ofertados em seu catálogo e por isso oferece um preço exclusivo dentro do mercado além de um alto controle de qualidade em todas as etapas da produção dos produtos. A <strong>venda de portas de aço </strong>da Central Portas atinge todo o território nacional e acompanha o kit de instalação com as instruções completas para que você possa ter seu produto totalmente funcional o mais rápido possível tendo em vista que só oferecemos serviços de instalação dentro do estado de São Paulo. Não compre portas ou portões de aço em outro lugar sem antes conhecer os modelos e as condições exclusivas com preço que cabe no seu bolso. Utilizamos nossa vasta experiência em conjunto com a qualidade de nossos profissionais para evoluir cada dia mais nossos serviços.</p>
<p>A Central Portas trabalha com aço desde 1999 e por possuir grande conhecimento nessa matéria prima pode oferecer produtos com ótimos custos e qualidade sem igual. Em 2013 nossa empresa voltou quase que completamente para a produção, <strong>venda de portas de aço</strong> e instalação (somente para região de São Paulo). Nossos representantes já realizaram <strong>venda de portas de aço </strong>para grandes empresas no mercado como Marisa, Besni, Taco Bell e diversos outros grandes nomes. Não perca tempo e faça seu orçamento sem compromisso e totalmente online para seu maior conforto através de nosso site. Consulte a área de atendimento de nossa instalação ou tire dúvidas sobre o kit de instalação ou instruções enviadas para compra em outros estados. A Central Portas oferece preços fora do comum por ser uma das maiores fabricantes deste produto dentro do Brasil. As Portas de aço podem ser utilizadas em comércios, lojas, empresas, indústrias, residências e diversos outros lugares.</p>
<h2><strong>A melhor venda de portas de aço bem próximo de você.</strong></h2>
<p>Consulte nossos atendentes para ter auxílio sobre <strong>venda de portas de aço </strong>e como solicitar sua encomenda para outros estados. Nossos profissionais são altamente treinados para realizar um atendimento gentil e prestativo com o selo de qualidade de nossa empresa.</p>
<h2><strong>Saiba mais sobre a venda de portas de aço.</strong></h2>
<p>Para eventuais dúvidas sobre a nossa <strong>venda de portas de aço </strong>entre em contato e seja prontamente auxiliado por um especialista para te atender da melhor maneira possível.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>