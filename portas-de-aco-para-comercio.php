<?php
    $title       = "Portas de Aço Para Comercio";
    $description = "As portas de aço para comércio também são uma ótima opção de segurança para proteger seu patrimônio, seus bens e sua mercadoria. Entre em contato com a nossa empresa e saiba mais!";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Encontre as melhores opções de <strong>portas de aço para comércio </strong>com um custo benefício incrível. A Central Portas é uma empresa que fabrica, produz, vende e instala (na região de São Paulo) portas e portões dos mais variados tipos com opções manuais e automáticas para atender aos diferentes requisitos procurados por nossos clientes. Trabalhamos com aço desde 1999 e por isso possuímos grande conhecimento nas propriedades e utilidades desse material. Em 2013 nossa empresa entrou forte no ramo da fabricação, produção, venda e instalação de portas e portões de aço com diversas opções manuais e automáticas prevendo o crescimento do ramo. Oferecemos diversos modelos com comando via botão ou via controle de rádio frequência além das versões de acionamento manual. Não feche sua compra de <strong>portas de aço para comércio </strong>em outro lugar sem antes conhecer as opções de portas e portões com condições exclusivas que somente a Central Portas oferece para seus clientes com produtos de altíssima qualidade.</p>
<p>A Central Portas é uma empresa que trabalha buscando oferecer uma solução prática, de alta qualidade e sem esquecer da importância da estética tendo em vista que as portas e portões muitas vezes podem ser a primeira impressão de um potencial cliente sobre seu segmento. Além de <strong>portas de aço para comércio </strong>você pode encontrar portas para residência com opções incríveis para assegurar sua residência de ótima maneira. As <strong>portas de aço para comércio </strong>também são uma ótima opção de segurança para proteger seu patrimônio, seus bens e sua mercadoria. Também contamos com diversas opções exclusivas para lojas que optam por exibir parcialmente sua vitrine mesmo com a porta fechada com total segurança e um design totalmente diferenciado criado por profissionais de alta capacidade para entregar os melhores resultados para os nossos clientes.</p>
<h2><strong>As melhores portas de aço para comércio estão próximas de você.</strong></h2>
<p>Nossas <strong>portas de aço para comércio </strong>podem ser encontradas em diversos locais. Dentre nossos clientes contamos com grandes empresas como Marisa, Taco Bell, Besni e muitos outros grandes nomes.</p>
<h2><strong>Saiba mais sobre as portas de aço para comércio.</strong></h2>
<p>Para saber mais, tirar dúvidas, solicitar pedidos ou orçamentos sobre nossas <strong>portas de aço para comércio </strong>entre em contato e seja prontamente auxiliado por um especialista para te atender de maneira sem igual e te auxiliar a encontrar a porta ideal para você.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>