<?php
    $title       = "Venda de Portas de Aço em São Paulo";
    $description = "Entre em contato com nossa venda de portas de aço em São Paulo e encomende agora mesmo a sua. Faça seu orçamento sem compromisso e totalmente online.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura pelo melhor lugar que ofereça <strong>venda de portas de aço em São Paulo </strong>encontrou o lugar certo para fazer suas cotações. A Central Portas é uma das maiores fabricantes de portas e portões de aço do Brasil. Trabalhamos com aço desde 1999 e por isso contamos com uma grande experiência e conhecimento dessa matéria prima. Nossa equipe é altamente treinada para oferecer um atendimento empático e respeitoso para satisfazer dúvidas e oferecer os melhores negócios quando se trata de <strong>venda de portas de aço em São Paulo. </strong>Não feche sua compra em outro lugar sem antes conhecer os produtos de alta qualidade com condições exclusivas que somente uma das maiores fabricantes do segmento pode oferecer para seus clientes. Todos os produtos disponíveis em nosso catálogo são de fabricação própria e por isso possuem custo direto de fábrica além de um alto controle de qualidade realizado por nossos profissionais em todas as etapas da produção do produto.</p>
<p>Encontre uma <strong>venda de portas de aço em São Paulo </strong>que cabe certinho com as necessidades que você precisa. O aço é um material de alta resistência e por isso pode ser exposto ao ar livre em diversas condições climáticas que ainda terá uma alta durabilidade. Além disso, sua alta resistência também é muito importante para proteger seu patrimônio e seus bens. Nossa <strong>venda de portas de aço em São Paulo </strong>atende diversas empresas como a Marisa, Taco Bell, Besni e outros grandes nomes do mercado, além de opções residenciais incríveis. Sabemos que em um produto desse a qualidade não é só o que importa, sendo que pode ser uma das primeiras impressões do ambiente e por isso oferecemos uma estética incrível em diferentes projetos de portões para agradar as mais variadas exigências de nossos clientes.</p>
<h2><strong>A melhor venda de portas de aço em São Paulo é na Central Portas.</strong></h2>
<p>Entre em contato com nossa <strong>venda de portas de aço em São Paulo </strong>e encomende agora mesmo a sua. Faça seu orçamento sem compromisso e totalmente online para sua maior comodidade através de nosso site.</p>
<h2><strong>A venda de portas de aço em São Paulo que você procura está aqui.</strong></h2>
<p>Para saber mais sobre nossa <strong>venda de portas de aço em São Paulo </strong>entre em contato com um de nossos atendentes para auxiliarmos no seu atendimento. Nossa equipe sempre está esperando seu contato em horário comercial para auxiliar no atendimento.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>