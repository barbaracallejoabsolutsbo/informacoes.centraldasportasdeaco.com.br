<?php
    $title       = "Fabricante de Portas de Aço em Pernambuco";
    $description = "Tenha um projeto exclusivo de porta automática basculante, de enrolar ou guilhotina com a Central das Portas de Aço. Executamos serviços de fabricante de portas de aço em Pernambuco e em todo o país. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Serviços especializados para portões em portas de aço em nossa <strong>fabricante de portas de aço em Pernambuco</strong>. Confeccionamos portões e portas de aço basculantes, RollUp ou de enrolar, guilhotina, automáticas ou manuais, entre outros artigos, com serviços de pintura, manutenção, reparo e consertos.</p>
<p>A Central das Portas de Aço oferece de forma exclusiva serviços especiais de nossa <strong>fabricante de portas de aço em Pernambuco</strong>, Piauí, São Paulo, Ceará, Barueri, Cajamar, Cotia, Guarulhos, Osasco, entre outros municípios e cidades e proximidades das regiões citadas.</p>
<p>Buscamos sempre suprir as expectativas dos clientes com produtos altamente satisfatórios e de alto padrão. Design exclusivo, equipamentos modernos, originais de fábrica, projetados para sua loja, residência, empresa ou qualquer que seja o espaço desejado. Conheça as melhores ofertas e condições disponíveis em nossa <strong>fabricante de portas de aço em Pernambuco</strong>. Atendimento para o Brasil todo, com condições especiais e serviços ainda mais especializados para público de São Paulo e capital.</p>
<p>Trabalhamos com as mais diversas estruturas, portas e portões em aço, automatizados ou manuais. Entre nossos artigos estão portas de enrolar, basculante, guilhotina, mezaninos, coberturas, acessórios para portões e portas de acesso com veículos para automatização como sensores, controles remotos, centrais de recepção de sinal e motores especiais para automação dessas estruturas. Fale com nosso atendimento, consulte nosso catálogo e garanta os melhores serviços e produtos encontrados na maior <strong>fabricante de portas de aço em Pernambuco</strong>.</p>
<h2><strong>Projetos exclusivos em nossa fabricante de portas de aço em Pernambuco</strong></h2>
<p>Tenha um projeto exclusivo de porta automática basculante, de enrolar ou guilhotina com a Central das Portas de Aço. Executamos serviços de<strong> fabricante de portas de aço em Pernambuco </strong>e em todo o país. Com condições e serviços especiais para clientes que são de São Paulo, nossa empresa fabrica portões e portas em aço com tecnologia moderna, design exclusivo e muita versatilidade e segurança. Contrate nossos serviços especializados para fabricação, instalação, manutenção e pequenos reparos em portas de aço em geral.</p>
<h2><strong>Compre com a maior fabricante de portas de aço em Pernambuco </strong></h2>
<p>Se você busca adquirir sua porta de aço automática, seja para sua loja, empresa, casa, condomínio, entre outros, a Central das Portas de Aço, atendendo o país todo, possui serviços exclusivos de confecção e<strong> fabricante de portas de aço em Pernambuco </strong>e Brasil todo. Consulte nossas condições especiais para outras cidades e localidades ou fale conosco para mais informações, orçamentos e projetos.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>