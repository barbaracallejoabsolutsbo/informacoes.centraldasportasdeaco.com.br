<?php
    $title       = "Fabricante de Portas de Aço no Ceará";
    $description = "Possuímos o melhor preço e custo benefício, pois somos fabricante de portas de aço no Ceará e fornecemos produtos com alto padrão de controle de qualidade para toda a região, distribuindo também para todo o Brasil.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A Central das Portas de Aço é uma empresa localizada em São Paulo que atende todo o Brasil em especialidades de confecção, reposição de peças, acessórios, equipamentos e manutenção de portas de aço de enrolar. Com preço incrivelmente acessível e justo, trabalhamos com mão de obra especializada e matéria prima de ponta. Consulte nossos serviços disponíveis em nosso catálogo e contrate a <strong>fabricante de portas de aço no Ceará</strong> e para o país inteiro.</p>
<p>Atendendo todo o Brasil, a <strong>fabricante de portas de aço no Ceará</strong>, Central das Portas de Aço é referência no ramo. Portões manuais ou automáticos com funcionamento remoto por controle ou acionamento manual, entre outras opções. Personalize seu projeto conosco, contrate nossos serviços e tenha todo o suporte desde a elaboração do projeto até a instalação e funcionamento do produto.</p>
<p>Manutenção também de portas de enrolar, portas guilhotina e portas basculantes, com preço baixo, peças originais, mão de obra especializada e orçamentos para o Brasil todo. <strong>Fabricante de portas de aço no Ceará</strong> é a Central das Portas de Aço.</p>
<p>Portões e portas de aço, estruturas industriais, coberturas e muito mais só aqui na maior <strong>fabricante de portas de aço no Ceará.</strong></p>
<h2><strong>Qualidade com preço baixo só na fabricante de portas de aço no Ceará número um</strong></h2>
<p>Possuímos o melhor preço e custo benefício, pois somos<strong> fabricante de portas de aço no Ceará </strong>e fornecemos produtos com alto padrão de controle de qualidade para toda a região, distribuindo também para todo o Brasil. Trabalhamos com aço galvanizado da melhor qualidade para confeccionar diversos artigos para sua segurança, proteção, comodidade e praticidade. Ofertas especiais para você que procura adquirir a sua porta de aço ou realizar a manutenção de suas estruturas feitas em aço.</p>
<h2><strong>Serviços com mão de obra especializada na fabricante de portas de aço do Ceará</strong></h2>
<p>Encontre os melhores serviços de fabricação, manutenção, reparos, reposição de peças, acessórios e equipamentos para automação de portas de enrolar, portas basculantes, entre outros artigos para ramo industrial, residencial ou comercial. Design requintado, luxuoso, portas altamente práticas, seguras e funcionais, mão de obra especializada e muito mais na melhor e mais especializada <strong>fabricante de portas de aço do Ceará.</strong></p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>