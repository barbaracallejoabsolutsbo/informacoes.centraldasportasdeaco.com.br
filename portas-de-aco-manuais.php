<?php
    $title       = "Portas de Aço Manuais";
    $description = "As portas de aço manuais são uma opção de baixo custo para que você possa proteger sua loja, comércio ou empresa de uma ótima forma.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura por <strong>portas de aço manuais </strong>encontrou um dos melhores lugares do Brasil para realizar sua cotação com os melhores preços. O aço é um material de alta resistência à água, calor, frio, vento e diversas condições climáticas que podem ser encontradas em nosso país e por isso contam com uma incrível durabilidade perante situações que facilmente degradam outros materiais. Além disso, o aço é muito resistente e capaz de aguentar fortes impactos e pode ser um ótimo recurso para proteger seu patrimônio e seus bens em caso de tentativa de invasão. Trabalhamos há mais de 20 anos com aço e por isso você pode encontrar uma empresa que conhece totalmente as propriedades do material que compõe seus principais produtos.  Encontre uma grande variedade de <strong>portas de aço manuais </strong>em nossa loja. Contamos com um ótimo desempenho através do grande trabalho de nossa equipe e entregamos produtos com ótimos prazos além de oferecer um ótimo serviço de instalação para o estado de São Paulo.</p>
<p>A Central Portas é uma das melhores empresas do segmento que oferece um tratamento totalmente diferenciado para todos os clientes potenciais que buscam nosso atendimento. Estamos desde 2013 trabalhando dentro da fabricação, vendas e instalação de portas nos antecipando ao crescimento potencial que o segmento apresentava diante da época em nossa região. As <strong>portas de aço manuais </strong>são uma opção de baixo custo para que você possa proteger sua loja, comércio ou empresa de uma ótima forma. O aço é um material extremamente resistente às mais diverso condições ambientais presentes em nosso país e por isso oferece uma alta durabilidade. Além disso, as <strong>portas de aço manuais </strong>podem ser facilmente manipuladas e são muito seguras tendo em vista a resistência do material inclusive para impactos em caso de uma possível tentativa de invasão.</p>
<h2><strong>A melhor loja de portas de aço manuais.</strong></h2>
<p>A Central Portas é uma das melhores lojas de <strong>portas de aço manuais </strong>por contar com uma venda direta da fábrica e por isso poder oferecer um preço excelente para seus clientes. Além disso, todos os produtos disponíveis em nosso catálogo são de fabricação própria, aumentando ainda mais o controle de qualidade de nossa empresa.</p>
<h2><strong>Peça já as suas portas de aço manuais com o melhor preço do mercado.</strong></h2>
<p>Para realizar orçamento ou pedidos de <strong>portas de aço manuais </strong>utilize nosso site de qualquer lugar para seu maior conforto. Para eventuais dúvidas entre em contato e seja prontamente auxiliado por um de nossos atendentes.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>