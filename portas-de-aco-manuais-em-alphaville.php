<?php
    $title       = "Portas de Aço Manuais em Alphaville";
    $description = "Faça seu orçamento de portas de aço manuais em Alphaville e consulte modelos totalmente sem compromisso através de nosso site. Entre em contato conosco!";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você busca pelas melhores <strong>portas de aço manuais em Alphaville </strong>encontrou o lugar ideal para realizar suas cotações de preços. A Central Portas é uma das mais recomendadas empresas no segmento, estando há muito tempo dentro do mercado e sempre buscando inovações para seus serviços e melhorar ainda mais os produtos oferecidos para nossos clientes. Encontre uma grande variedade de opções de <strong>portas de aço manuais em Alphaville </strong>com os melhores preços da região. Trabalhamos com produtos de fabricação própria aumentando ainda mais o vantajoso custo para nossos clientes por ser uma venda realizada diretamente da fábrica e mantendo um alto controle de qualidade oferecido por nossos profissionais. Nossos produtos mantêm um ótimo desempenho e agradam os mais exigentes clientes que já buscaram comprar em nossa loja e sentiram-se plenamente satisfeitos com o produto que receberam. Não feche sua compra de portas ou portões de aço em outro lugar sem antes conhecer os preços e condições exclusivas que somente uma grande empresa do segmento pode oferecer para os clientes.</p>
<p>A Central Portas atua no ramo de aço desde 1999 e em 2013 se especializou em diversos tipos de portas e portões manuais e automáticos, antecipando o crescimento potencial do segmento em nossa região. Encontre o modelo ideal em nossas <strong>portas de aço manuais em Alphaville </strong>para atender exatamente a necessidade que você tenha independente da metragem disponível.  O aço é um material extremamente resistente que pode aguentar diversas condições climáticas diferentes de outros materiais de fácil degradação quando expostos a esses fatores. Além disso, as <strong>portas de aço manuais em Alphaville </strong>são uma ótima maneira de proteger seu patrimônio e seus bens, tendo em vista que é um material de alta resistência com um desempenho incrível em sua função.</p>
<h2><strong>Saiba mais sobre as portas de aço manuais em Alphaville.</strong></h2>
<p>Faça seu orçamento de <strong>portas de aço manuais em Alphaville </strong>e consulte modelos totalmente sem compromisso através de nosso site. Conheça as condições que cabem no seu bolso para comprar a porta de aço que você precisa.</p>
<h2><strong>As melhores portas de aço manuais em Alphaville.</strong></h2>
<p>Para saber mais sobre nossas <strong>portas de aço manuais em Alphaville </strong>ou quaisquer outros produtos entre em contato e seja prontamente atendido por um especialista para te auxiliar da melhor maneira possível com o maior profissionalismo encontrado no mercado.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>