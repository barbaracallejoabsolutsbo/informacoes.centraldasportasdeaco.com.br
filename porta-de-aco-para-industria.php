<?php
    $title       = "Porta de Aço Para Industria";
    $description = "Diretamente da fábrica, adquira com a Central das Portas de Aço a mais notável e segura porta de aço para indústria disponível do mercado nacional.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Diretamente da fábrica, adquira com a Central das Portas de Aço a mais notável e segura <strong>porta de aço para indústria </strong>disponível do mercado nacional. Fabricamos, reparamos, consertamos e fazemos portas de aço para todo o Brasil. Solicite já orçamentos com nossos atendentes e garanta a melhor qualidade de serviço de fabricação e suporte técnico para portas de enrolar e portas de aço para indústria, comércios, lojas, entre outros.</p>
<p>A <strong>porta de aço para indústria, </strong>feita em aço galvanizado, um dos mais resistentes e duráveis metais, garante toda segurança e usabilidade com praticidade para sua casa, comércio, loja e ou para qualquer um espaço desejado para instalação. Pode contar por acionamento automático, remoto, por controle ou operador, com diversos acessórios para personalização, disponíveis aqui na Central das Portas de Aço.</p>
<p>A <strong>porta de aço para indústria</strong> tem muita usabilidade quando instalada em comércios, lojas, shoppings e inclusive em residências, estando disponível nos modelos porta de enrolar, porta basculante ou porta guilhotina, feitas em aço, proporcionam segurança para seu patrimônio com preço altamente acessível, muita segurança e vantagens exclusivas. Pode contar com acionamento da porta manual ou automático e personalizações que só se encontram aqui.</p>
<p>Consulte já as melhores condições para adquirir sua <strong>porta de aço para indústria</strong> e conheça mais sobre o produto e sobre nossa empresa.</p>
<h2><strong>Central das Portas de Aço: Fabricante de porta de aço para indústria</strong></h2>
<p>A Central das Portas de Aço atende grandes marcas no Brasil inteiro. Trabalhamos com todo profissionalismo para fabricar peças únicas, exclusivas, com design luxuoso, acabamento impecável e todo suporte pós venda ao adquirir sua<strong> porta de aço para indústria </strong>conosco. Contamos com o preço mais justo do mercado pois somos os fabricantes das nossas próprias portas, oferecemos serviços com excelência e primor. Contacte-nos.</p>
<h2><strong>Porta de aço para indústria, residências, lojas, entre outros estabelecimentos</strong></h2>
<p>Efetuamos projetos, instalações, manutenções e consertos em portas de aço para indústrias, residências, lojas e muitos outros locais e estabelecimentos, com todo suporte de profissionais especialistas de nossa assistência técnica para<strong> porta de aço para indústria. </strong>Consulte já nosso catálogo, veja as melhores condições e ofertas para estar comprando sua <strong>porta de aço para indústria </strong>com a maior fabricante desses tipos de artigos, Central das Portas de Aço.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>