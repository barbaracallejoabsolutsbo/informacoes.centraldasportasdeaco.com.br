<?php
    $title       = "Conserto de Portas de Enrolar";
    $description = "Com a nossa empresa você encontra manutenção especializada para conserto de portas de enrolar, sejam para residências, comércios, empresas ou indústrias. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Com a nossa empresa você encontra manutenção especializada para <strong>conserto de portas de enrolar</strong>, sejam para residências, comércios, empresas ou indústrias. A Central das Portas de Aço fornece produtos de qualidade comprovada, proporcionando toda a versatilidade e segurança ao utilizar de nossos artigos e serviços de reparo e manutenção, com mão de obra especializada.</p>
<p>As portas de enrolar são comumente utilizadas em comércios, fábricas, indústrias e inclusive em residências. Em diversos modelos e acabamentos, podem ser automáticas, personalizadas com acessórios como sensores infravermelho, controle, entre outros, para conforto dos usuários. Mas em diversos casos, clientes optam por portas manuais. Para sempre se manterem seguras e confiáveis, faça o <strong>conserto de portas de enrolar</strong> de forma preventiva periodicamente com a Central das Portas de Aço.</p>
<p>Esses tipos de portas podem funcionar de forma automática com motores de alta potência, dessa forma, facilitam os processos de abrir e fechar as portas de enrolar com até 15 m de comprimento. A Central das Portas de Aço conta com todo suporte profissional para <strong>conserto de portas de enrolar</strong>, sejam automáticas ou manuais.</p>
<p>Além de <strong>conserto de portas de enrolar, </strong>trabalhamos com porta basculante, guilhotina, entre outros artigos que você encontra em nosso catálogo.</p>
<h2><strong>Encontre o melhor custo benefício para</strong> <strong>conserto de portas de enrolar</strong></h2>
<p>A Central das Portas de Aço trabalha com todo profissionalismo, peças originais e de alto padrão de qualidade, para serviços de<strong> conserto de portas de enrolar</strong> garantindo segurança e funcionamento adequado. Para residências, comércios, indústrias, entre outros, oferecemos todo suporte para suas portas de enrolar, e também para diversos outros modelos. Consulte nosso catálogo de serviços e informações e encontre o que você procura. O melhor custo benefício do mercado regional para você.</p>
<h2><strong>Possibilidades de conserto de portas de enrolar com a Central das Portas de Aço</strong></h2>
<p>Nossa empresa localizada em São Paulo efetua manutenções, reparos e<strong> conserto de portas de enrolar </strong>para toda região com o melhor preço do mercado. Consertamos mecanismos de portas manuais ou automáticas, troca de fechadura, trava, motor, peças em geral, originais de fábrica e certificadas. Entre em contato conosco e solicite um orçamento e mais informações para todos os tipos de reparos e consertos para portas de aço de enrolar.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>