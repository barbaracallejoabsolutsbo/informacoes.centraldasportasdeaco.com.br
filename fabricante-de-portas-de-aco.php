<?php
    $title       = "Fabricante de Portas de Aço";
    $description = "Nossa fabricante de portas de aço, Central das Portas de Aço, dispõe de diversos itens e artigos como acessórios para agregar a funcionalidade se seus portões e portas.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A Central das Portas de Aço é uma <strong>fabricante de portas de aço</strong> que trabalha com a melhor matéria prima para fabricação de portas e portões automáticos e manuais para você. Se você busca adquirir sua porta de enrolar de aço aqui é o local correto. Consulte nossas condições para confecção, instalação, manutenção e serviços especializados para portas de aço para toda São Paulo, cidades próximas e Brasil.</p>
<p>Além de portas de enrolar, trabalhamos com portas de aço em geral, entre elas, basculantes, guilhotina e muito mais. Serviços de fabricação, manutenção e reparo para todos esses modelos, com peças de reposição e acessórios como controle remoto, sensores, receptores, motores automatizados e muito mais. A <strong>fabricante de portas de aço</strong> que tem o melhor preço de São Paulo entregando uma qualidade incrível com prazos incríveis, confira nossos produtos.</p>
<p>Nossa <strong>fabricante de portas de aço</strong> trabalha com portões rollup ou rollmatic, conhecidos como portões ou portas de enrolar, que funcionam muito bem para estabelecimentos, lojas, empresas e indústrias, e também podem ser utilizados em casas em modelos mais arrojados e com design requintado. Além disso, você encontra portões basculantes, portões guilhotina e muito mais. Produtos disponíveis em nosso catálogo.</p>
<p>Atuando no ramo a anos, nossa <strong>fabricante de portas de aço</strong> conta com serviços completos para suporte com manutenção, reparo, reposição de peças, revisão e muito mais, para que os seus portões, automáticos ou manuais, sempre estejam com bom funcionamento, sem falhas.</p>
<h2><strong>Segurança e praticidade com nossos produtos, fabricante de portas de aço</strong></h2>
<p>Nossa<strong> fabricante de portas de aço</strong>, Central das Portas de Aço, dispõe de diversos itens e artigos como acessórios para agregar a funcionalidade se seus portões e portas. Entre eles estão sensores infravermelho, controle remoto para abrir, fechar ou parar os portões, sensores diversos, centrais de controle e muito mais. Fale conosco para orçamentos e preços.</p>
<h2><strong>Fabricante de portas de aço com serviços especializados para São Paulo</strong></h2>
<p>Encontre serviços especializados em nossa<strong> fabricante de portas de aço</strong>. Com clientes no Brasil todo, trabalhamos com produtos de altíssima qualidade garantindo sempre um bom funcionamento e segurança. Entre nossos clientes estão as lojas da Oi, Besni, Marisa, Taco Bell, Lojas Rede, Libercon Engenharia, entre outras empresas. Com produtos bem acabados, de ótima matéria prima, excelente qualidade de equipamentos e designs belos, funcionais e exclusivos, nossa fábrica é experiente no ramo. Contrate nossos serviços para portas e portões de aço.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>