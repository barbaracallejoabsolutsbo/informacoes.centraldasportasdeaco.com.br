<?php
    $title       = "Controle Para Portas de Enrolar";
    $description = "O controle para portas de enrolar nada mais é do que um controle remoto para acionamento do motor que movimenta as portas. Entre em contato e saiba mais!";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A Central das Portas de Aço trabalha com confecção e instalação de portas de aço, como portas guilhotina, de enrolar e basculante, para residências, empresas e lojas. Vendemos também, acessórios para automatização de portas como <strong>controle para portas de enrolar</strong>, botoeiras de acionamento do motor dos portões e portas, sensores de infravermelho, entre outros.</p>
<p>Prezando sempre pela melhor qualidade dos produtos, atendimento e satisfação do cliente, a Central das Portas de Aço trabalha com produtos originais de fábrica, novos e de alto padrão, com controle de qualidade rigoroso. Com todo suporte pós venda ou pós-serviço, a Central das portas de aço vende <strong>controle para portas de enrolar</strong> para automação de suas portas. Confira.</p>
<p>O <strong>controle para portas de enrolar</strong> nada mais é do que um controle remoto para acionamento do motor que movimenta as portas. Para segurança e praticidade, esse dispositivo existe para que o portão seja acionado a distância quando o botão é acionado, dessa forma, liberando acesso para o usuário ao local desejado.</p>
<p>O <strong>controle para portas de enrolar</strong> também é comumente utilizado em condomínios e residências para acionamento dos portões para acesso com carros, para facilitar a identificação do morador e liberar o acesso com mais agilidade. O receptor do sinal fica em uma caixa protegida que pode ser instalada em qualquer local dentro da recomendação para funcionamento adequado da distância do acionamento do controle.</p>
<h2><strong>Controle para portas de enrolar de qualidade incomparável</strong></h2>
<p>Está cansado de ter que trocar o controle de acionamento para suas portas e portões pois quebram ou gastam a bateria com muita rapidez? Conheça o<strong> controle para portas de enrolar </strong>disponível na Central das Portas de Aço. Esse dispositivo, quando acionado, envia um comando por rádio com frequência para uma central eletrônica que fica junto dos motores automatizadores da porta de enrolar ou portão basculante. Dessa forma, o sinal recebido pode ser utilizado para abrir, fechar, travar o portão. Além de muito resistente e durável, economiza bateria e não a consome em pouco tempo.</p>
<h2><strong>Compre controle para portas de enrolar e muitos outros acessórios e peças</strong></h2>
<p>Na Central das Portas de Aço você encontra muitos acessórios para portas de enrolar. Além de nosso <strong>controle para portas de enrolar</strong>, aqui você encontra peças de reposição originais de fábrica, sensores de infravermelho, motores para automação dos portões e portas, além das próprias portas de enrolar. Fale com nosso atendimento e faça um orçamento para serviços e produtos.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>