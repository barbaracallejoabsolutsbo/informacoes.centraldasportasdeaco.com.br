<?php
    $title       = "Portas de Enrolar em Minas Gerais";
    $description = "As portas de enrolar em Minas Gerais acompanham o kit de instalação na entrega para que nosso cliente possa ser auxiliado com todas as instruções tendo em vista que somente oferecemos o serviço de instalação dentro do estado de São Paulo.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura pelas melhores opções de <strong>portas de enrolar em Minas Gerais </strong>encontrou o lugar ideal com os melhores preços que cabem no seu bolso. As portas de rolar são uma ótima opção para lojas, empresas e muitas vezes até para residências. Essas portas costumam ter um funcionamento muito útil embora muito simples e ocupam o mínimo espaço por serem enroladas quando necessário abrir. As <strong>portas de enrolar em Minas Gerais </strong>acompanham o kit de instalação na entrega para que nosso cliente possa ser auxiliado com todas as instruções tendo em vista que somente oferecemos o serviço de instalação dentro do estado de São Paulo. A Central Portas é uma empresa de alta confiabilidade dentro do ramo de produção, entrega e instalação de portas de portões de aço. Trabalhando com aço desde 1999 entramos no ramo de portas e portões em 2013 utilizando nossa grande experiência nessa matéria prima para oferecer grandes opções para nossos clientes.</p>
<p>A Central Portas é uma empresa que possui uma grande gama de opções de portas e portões de aço de grande qualidade. Contamos com opções manuais e automáticas que possuem a opção de acionamento por botão ou por controle remoto de rádio frequência. As <strong>portas de enrolar em Minas Gerais </strong>são de grande resistência e podem ser expostas em diversas condições climáticas tendo em vista que o aço é um material que lida muito bem com a degradação derivada dessas condições. Além disso, o aço é um material resistente e por isso é uma ótima opção como escolha da primeira barreira de proteção de seu patrimônio. Sabemos também que além de segurança as <strong>portas de enrolar em Minas Gerais </strong>devem oferecer um ótimo designer para enaltecer a fachada do ambiente sendo que é uma dos primeiros pontos perceptíveis do local.</p>
<h2><strong>As melhores opções de portas de enrolar em Minas Gerais estão aqui. </strong></h2>
<p>Somente na Central Portas você encontra a opção ideal das <strong>portas de enrolar em Minas Gerais </strong>com condições exclusivas de pagamento. Realize seu orçamento totalmente online e sem compromisso através de nosso site.</p>
<h2><strong>Saiba mais sobre as portas de enrolar em Minas Gerais.</strong></h2>
<p>Para eventuais dúvidas sobre as <strong>portas de enrolar em Minas Gerais </strong>ou quaisquer outros produtos entre em contato e seja prontamente atendido por um de nossos especialistas para te auxiliar da melhor maneira possível.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>