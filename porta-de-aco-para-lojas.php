<?php
    $title       = "Porta de Aço Para Lojas";
    $description = "Entre os diversos modelos comuns de portas para lojas, a porta de aço para lojas é um dos modelos mais seguros, funcionais e práticos de todos.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Entre os diversos modelos comuns de portas para lojas, a <strong>porta de aço para lojas</strong> é um dos modelos mais seguros, funcionais e práticos de todos. Podendo ser combinado com outras portas, a <strong>porta de aço para lojas</strong> pode ser de enrolar, automatizada ou manual, com toda segurança e praticidade do mercado. Um artigo moderno com tecnologia ímpar para conforto e versatilidade ao abrir e fechar os acessos das dependências da sua loja ou comércio, garantindo toda a segurança do ambiente.</p>
<p>Conheça nossa empresa, a Central das Portas de Aço é uma fabricante de portas de aço para indústria, residência e comércios e disponibiliza diversos modelos de <strong>porta de aço para lojas, </strong>entre elas de enrolar, muito comum entre comércios, lojas de rua ou de shopping, supermercados, entre outros locais, porta basculante e porta guilhotina, mais comuns em condomínios, empresas e grandes indústrias.</p>
<p>A <strong>porta de aço para lojas</strong>, ou porta de aço RollUp ou de enrolar, é um tipo de porta que possui mecanismo retrátil que pode ser acionado por um motor que efetua esse percurso automaticamente ou pode ser feito manualmente, com as mãos. Se você já possui uma porta de aço de enrolar e quer automatizar, aqui é possível adquirir todo o sistema e a mão de obra para serviço de adaptação e instalação.</p>
<h2><strong>Qual o diferencial dos modelos encontrados de porta de aço para lojas conosco?</strong></h2>
<p>As portas fabricadas conosco podem ser encontradas em 3 tipos de modelos, basculantes, guilhotina e de enrolar ou RollUp. Todos os modelos servem muito bem a muitos tipos de instalações como empresas, comércios, indústrias e até residências. As portas de enrolar podem ser encontradas em modelos vazados ou totalmente fechados, e todas essas estruturas feitas em aço podem contar com pintura eletrostática para personalização. Além disso, trabalhamos com mezaninos e coberturas industriais, e muitos outros artigos que só se encontram na Central das Portas de Aço, <strong>porta de aço para lojas</strong> com o melhor preço e qualidade do Brasil.</p>
<h2><strong>Como escolher a porta de aço para lojas ideal?</strong></h2>
<p>A chapa da <strong>porta de aço para lojas</strong> de enrolar pode ser vazada ou simples (fechado), de acordo com o gosto do cliente. Pode ser personalizada para contar com acionamento manual ou automático por motor, dessa forma, poupa esforço do usuário. Normalmente esse tipo de porta é usada para proteger algum comércio e loja, por isso é muito resistente e pesada. Contar com abertura por motor e acionamento remoto certamente é uma grande praticidade, e pode ser feito facilmente com a Central das Portas de Aço. Contrate nossos serviços.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>