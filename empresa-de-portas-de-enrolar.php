<?php
    $title       = "Empresa de Portas de Enrolar";
    $description = "Nossa empresa de portas de enrolar também trabalha com toda gama de acessórios para portas de aço automatizadas como controles remoto, sensores, motores, entre outros.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Especializados, a Central das Portas de Aço é uma <strong>empresa de portas de enrolar</strong> que trabalha com muito primor e qualidade buscando sempre a satisfação do cliente. Oferecemos produtos e serviços que garantem o bom funcionamento das suas portas e portões, funcionando de forma eficiente com segurança e versatilidade.</p>
<p>Em nossa <strong>empresa de portas de enrolar</strong>, nossos clientes podem optar qual tipo de porta deseja em seu estabelecimento, empresa ou lar. Portas disponíveis em modelos automáticos ou manuais em aço galvanizado de altíssima qualidade. Confeccionamos, instalamos e prestamos manutenção para diversos tipos de portas de aço.</p>
<p>Nossas portas de aço automáticas são feitas de material resistente, durável e de altíssima qualidade. Isso porque visamos sempre sua segurança, praticidade e versatilidade ao utilizar de nossos produtos disponíveis para lojas, casas, empresas, condomínios, entre outros. Nossa <strong>empresa de portas de enrolar</strong> também trabalha com toda gama de acessórios para portas de aço automatizadas como controles remoto, sensores, motores, entre outros.</p>
<p>Acesse o catálogo da Central das Portas de Aço para mais informações da maior <strong>empresa de portas de enrolar</strong> de São Paulo. Tudo o que você procura sobre portas de aço manuais ou automáticas está aqui.</p>
<h2><strong>Empresa de portas de enrolar, serviços de fábrica e de manutenção</strong></h2>
<p>Trabalhamos com todos acessórios, peças de reposição e muito mais para portas de aço, modelos diversos. Encontre todo suporte para manutenção, compra e instalação de portas de aço, basculantes, de enrolar ou guilhotina, portões automáticos para praticidade de acesso com veículos, entre outros. Consulte nossos produtos e serviços e conheça a maior<strong> empresa de portas de enrolar </strong>de aço de São Paulo.</p>
<h2><strong>Conheça nossa empresa de portas de enrolar, Central das Portas de Aço</strong></h2>
<p>A melhor opção de<strong> empresa de portas de enrolar </strong>para você. Preços exclusivos para diversos serviços e produtos. Compre sua porta de enrolar, porta basculante ou porta guilhotina, artigos em aço e muito mais para seu conforto e segurança. Automatização de portas e portões. Confira nosso catálogo e veja os serviços e produtos que mais lhe agradam. Contrate nossos serviços com mão de obra especializada, profissionais experientes, ágeis e que realizam serviços com muita precisão e dedicação, preservando o local onde os serviços podem ser efetuados.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>