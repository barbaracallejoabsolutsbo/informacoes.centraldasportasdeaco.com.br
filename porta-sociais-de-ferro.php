<?php
    $title       = "Porta Sociais de Ferro";
    $description = "Na Central das Portas de Aço você encontra portas de enrolar, porta sociais de ferro, portas basculantes, portas guilhotina, mezaninos, estruturas industriais como coberturas, entre outros. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A <strong>porta sociais de ferro</strong> galvanizado de melhor qualidade está aqui. Conheça a Central das Portas de Aço, líder no ramo de portas e portões de aço oferece serviços de fabricação, manutenção e reposição de peças para esses e outros tipos de estruturas em metais e aço galvanizado.</p>
<p>Podendo ser utilizada em residências, lojas, escritórios, consultórios, entre outros locais, a <strong>porta sociais de ferro</strong> é feita de aço galvanizado, resistente ao tempo e a todas variantes climáticas como umidade, chuva, granizo, entre outros. O portão comprado conosco vai pronto para instalação e todo o suporte para esse tipo de serviço você também encontra aqui, com mão de obra especializada e preço justo.</p>
<p>É importante toda uma preparação para instalar as portas sociais de ferro, portanto, mão de obra especializada nunca é demais. Contrate nossos serviços e agende uma visita técnica para projetos e serviços de instalação e reparo de portões e <strong>porta sociais de ferro</strong> para todo o Brasil.</p>
<p>Com garantia e toda atenção pós venda, a melhor <strong>porta sociais de ferro</strong> do Brasil está na Central das Portas de Aço. Fale conosco e contrate o serviço de maior qualidade do mercado para portas e portões de aço.</p>
<h2><strong>Compre porta sociais de ferro novas com a Central das Portas de Aço</strong></h2>
<p>Na Central das Portas de Aço você encontra portas de enrolar,<strong> porta sociais de ferro, </strong>portas basculantes, portas guilhotina, mezaninos, estruturas industriais como coberturas, entre outros. Todos os materiais são novos de fábrica e confeccionados pela Central das Portas de Aço, em material certificado e de alto padrão de qualidade. Contrata os serviços especializados de nossa empresa e tenha portas e portões de ferro ou aço em seu comércio ou residência. Com qualidade padrão Premium, atendemos o Brasil inteiro para envio de nossos artigos. Confira nosso catálogo.</p>
<h2><strong>Atendemos muitas regiões com envio por transportadora para porta sociais de ferro</strong></h2>
<p>Com envio para praticamente todo o Brasil, fabricamos e enviamos<strong> porta sociais de ferro </strong>para todo o país, com preço altamente acessível e justo, fazendo valer cada centavo do seu investimento. Transporte totalmente seguro e atencioso para que seus artigos cheguem totalmente intactos com agilidade e pontualidade.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>