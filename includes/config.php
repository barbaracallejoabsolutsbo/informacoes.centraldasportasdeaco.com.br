<?php

    // Principais Dados do Cliente
    $nome_empresa = "Central das Portas de Aço";
    $emailContato = "contato@centraldasportasdeaco.com.br";

    // Parâmetros de Unidade
    $unidades = array(
        1 => array(
            "nome" => "Central das Portas de Aço",
            "rua" => "Av. Régis, 600",
            "bairro" => "Cidade Jardim Cumbica",
            "cidade" => "Guarulhos",
            "estado" => "São Paulo",
            "uf" => "SP",
            "cep" => "07180-120",
            "latitude_longitude" => "", // Consultar no maps.google.com
            "ddd" => "11",
            "telefone" => "2479-8148",
            "whatsapp" => "99798-5954",
            "link_maps" => "" // Incorporar link do maps.google.com
        ),
        2 => array(
            "nome" => "",
            "rua" => "",
            "bairro" => "",
            "cidade" => "",
            "estado" => "",
            "uf" => "",
            "cep" => "",
            "ddd" => "",
            "telefone" => ""
        )
    );
    
    // Parâmetros para URL
    $padrao = new classPadrao(array(
        // URL local
        "http://localhost/informacoes.centraldasportasdeaco.com.br/",
        // URL online
        "https://www.informacoes.centraldasportasdeaco.com.br/"
    ));
    
    // Variáveis da head.php
    $url = $padrao->url;
    $canonical = $padrao->canonical;
	
    // Parâmetros para Formulário de Contato
    $smtp_contato            = ""; // Solicitar ao líder do dpto técnico, 177.85.98.119
    $email_remetente         = ""; // Criar no painel de hospedagem, admin@...
    $senha_remetente         = "c0B1S3vH5eCvAO";

    // Contato Genérico (para sites que não se hospedam os e-mails)
    // $smtp_contato            = "111.111.111.111";
    // $email_remetente         = "formulario@temporario-clientes.com.br";
    // $senha_remetente         = "4567FGHJK";

    // Recaptcha Google
    $captcha                 = false; // https://www.google.com/recaptcha/
    $captcha_key_client_side = "";
    $captcha_key_server_side = "";

    // CSS default
    $padrao->css_files_default = array(
        "default/reset",
        "default/grid-system",
        "default/main",
        "default/slicknav-menu",
        "_main-style"
    );
    
    // JS Default
    $padrao->js_files_default = array(
        "default/jquery-1.9.1.min",
        "default/modernizr",
        "default/jquery.slicknav.min",
        "jquery.padrao.main"
    );
        
    // Listas de Palavras Chave
    $palavras_chave = array(
"Portão de Aço Industriais",
"Portão de Aço Industriais em São Paulo",
"Portão de Aço Industriais em Guarulhos",
"Portão de Aço Industriais em Alphaville",
"Conserto de Portas de Enrolar",
"Conserto de Portas de Enrolar em Guarulhos",
"Conserto de Portas de Enrolar em São Paulo",
"Controle Para Portas de Enrolar",
"Portas de Aço Manuais",
"Portas de Aço Manuais em São Paulo",
"Portas de Aço Manuais em Guarulhos",
"Portas de Aço Manuais em São José dos Campos",
"Portas de Aço Manuais em Alphaville",
"Portas de Aço Manuais no Piauí",
"Empresa de Portas de Enrolar",
"Fabricante de Portas de Aço",
"Fabricante de Portas de Aço em Guarulhos",
"Fabricante de Portas de Aço em Pernambuco",
"Fabricante de Portas de Aço em Piauí",
"Fabricante de Portas de Aço em São Paulo",
"Fabricante de Portas de Aço no Ceará",
"Fabricante de Portas de Enrolar",
"Manutenção de Portas de Enrolar",
"Manutenção de Portas de Enrolar em São Paulo",
"Manutenção de Portas de Enrolar em Guarulhos",
"Manutenção de Portas de Enrolar em Barueri",
"Manutenção de Portas de Enrolar em Cajamar",
"Manutenção de Portas de Enrolar em Cotia",
"Manutenção de Portas de Enrolar em Osasco",
"Porta Sociais de Ferro",
"Porta de Aço Para Lojas",
"Parta de Aço Para Lojas em São Paulo",
"Porta de Aço Para Lojas em Guarulhos",
"Porta Comercial de Enrolar Automática",
"Porta de Aço Para Industria",
"Portas de Aço Automática Preço",
"Portas de Aço Para Comercio",
"Portas de Aço Para Shoppings",
"Portas de Enrolar em Espirito Santo",
"Portas de Enrolar em Minas Gerais",
"Portas de Enrolar em São Paulo",
"Portas de Enrolar em Sorocaba",
"Portas de Enrolar no Maranhão",
"Portas de Enrolar Preço",
"Venda de Portas de Aço",
"Venda de Portas de Aço no Piauí",
"Venda de Portas de Aço em Guarulhos",
"Venda de Portas de Aço em São Paulo",
"Venda de Portas de Aço no Maranhão",
"Venda de Portas de Aço em Sorocaba"
    );
   
    $palavras_chave_com_descricao = array(
        "Item 1" => "Lorem ipsum dolor sit amet.",
        "Item 2" => "Laudem dissentiunt ut per.",
        "Item 3" => "Solum repudiare dissentiunt at qui.",
        "Item 4" => "His at nobis placerat.",
        "Item 5" => "Ei justo lucilius nominati vim."
    );
    
     /**
     * Submenu
     * 
     * $opcoes = array(
     * "id" => "",
     * "class" => "",
     * "limit" => 9999,
     * "random" => false
     * );
     * 
     * $padrao->subMenu($palavras_chave, $opcoes);
     * 
     */

    /**
     * Breadcrumb
     * 
     * -> Propriedades
     * 
     * Altera a url da Home no breadcrumb
     * $padrao->breadcrumb_url_home = "";
     * 
     * Altera o texto que antecede a Home
     * $padrao->breadcrumb_text_before_home = "";
     * 
     * Altera o texto da Home no breadcrumb
     * $padrao->breadcrumb_text_home = "Home";
     * 
     * Altera o divisor de níveis do breadcrumb
     * $padrao->breadcrumb_spacer = " » ";
     * 
     * -> Função
     * 
     * Cria o breadcrumb
     * $padrao->breadcrumb(array("Informações", $h1));
     * 
     */

    /**
     * Lista Thumbs
     * 
     * $opcoes = array(
     * "id" => "",
     * "class_div" => "col-md-3",
     * "class_section" => "",
     * "class_img" => "img-responsive",
     * "title_tag" => "h2",
     * "folder_img" => "imagens/thumbs/",
     * "extension" => "jpg",
     * "limit" => 9999,
     * "type" => 1,
     * "random" => false,
     * "text" => "",
     * "headline_text" => "Veja Mais"
     * );
     * 
     * $padrao->listaThumbs($palavras_chave, $opcoes);
     * 
     */
    
    /**
     * Funções Extras
     * 
     * $padrao->formatStringToURL();
     * Reescreve um texto em uma URL válida
     * 
     */